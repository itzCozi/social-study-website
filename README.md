# Social Study's Project Website

**CURRENTLY THE REPOSITORY FOR THIS PROJECT IS ON [GITLAB](https://gitlab.com/itzCozi/social-study-website) DUE TO GITHUB ISSUES WITH MY ACCOUNT**

### Official Site
The official site (the one i host) is hosted on [CloudFlare Pages](https://pages.cloudflare.com/) at https://social-study-website.pages.dev/

### Run Locally
Type the following commands into your terminal / command line to run the website locally at http://localhost:8080.
```bash
git clone https://gitlab.com/itzCozi/social-study-website.git
cd social-study-website
npm install
npm run start
```
Then simply visit http://localhost:8080 in your browser to see the webpage!
